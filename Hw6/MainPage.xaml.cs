﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Hw6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void GetDef(object sender, EventArgs args)
        {
            if (CrossConnectivity.Current.IsConnected)//check for internet connectivity
            {
                HttpClient client = new HttpClient();//create client


                var uri = new Uri(string.Format($"https://owlbot.info/api/v4/dictionary/" + $"{DictEntry.Text}"));//format uri with word we want definition of 
                var request = new HttpRequestMessage();//create request
                request.Method = HttpMethod.Get;//make it a get request
                request.RequestUri = uri;//add uri to request

                HttpResponseMessage response = await client.SendAsync(request);//send request
                Words word = null;//initiate empty variable to hold C# response item
                if (response.IsSuccessStatusCode)//check if result was valid
                {
                    var content = await response.Content.ReadAsStringAsync();//read result into new var
                    word = Words.FromJson(content);//convert json string to c# object
                    if (word!=null)
                    {
                        HW6.ItemsSource = word.Definitions;
                    }
                    else { await DisplayAlert("Error", "You did not recieve any content", "Try Again"); }
                }
                else { //if error in response
                    await DisplayAlert("Error", "No Such Word", "Try Again");
                }
            }
            else//not connected to internet, display error message
            {
                await DisplayAlert("Error", "Connect to the internet", "OK");
            }
        }
        void Handle_Refreshing(System.Object sender, System.EventArgs e)//do nothing on refresh
        {
            HW6.IsRefreshing = false;
        }
    }
        public partial class Words //copy and pasted from website
        {
            [JsonProperty("definitions")]
            public Definition[] Definitions { get; set; }

            [JsonProperty("word")]
            public string Word { get; set; }

            [JsonProperty("pronunciation")]
            public string Pronunciation { get; set; }
        }

        public partial class Definition
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("definition")]
            public string DefinitionDefinition { get; set; }

            [JsonProperty("example")]
            public object Example { get; set; }

            [JsonProperty("image_url")]
            public Uri ImageUrl { get; set; }

            [JsonProperty("emoji")]
            public object Emoji { get; set; }
        }

        public partial class Words
        {
            public static Words FromJson(string json) => JsonConvert.DeserializeObject<Words>(json, Hw6.Converter.Settings);
        }

        public static class Serialize
        {
            public static string ToJson(this Words self) => JsonConvert.SerializeObject(self, Hw6.Converter.Settings);
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }

    }


